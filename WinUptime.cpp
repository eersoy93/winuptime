﻿// Copyright(c) 2023 Erdem Ersoy (eersoy93)
//
// Licensed with MIT license. See LICENSE file for full text.

#include <windows.h>
#include <iostream>

int main()
{
    ULONGLONG TickCount = GetTickCount64();

    TickCount /= 1000;

    unsigned int seconds = TickCount % 60;
    TickCount /= 60;

    unsigned int minutes = TickCount % 60;
    TickCount /= 60;

    unsigned int hours = TickCount % 24;

    unsigned int days = TickCount / 24;

    std::cout << "The system has been up for: " << days << " days, "
        << hours << " hours, " << minutes << " minutes, " << seconds << " seconds."
        << std::endl;

    return EXIT_SUCCESS;
}
