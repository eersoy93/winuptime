# WinUptime
A simple system uptime viewer on console written with C++ and Windows API.

# Author
Erdem Ersoy (eersoy93) (with the help of ChatGPT)

# Copyright and License
Copyright (c) 2023 Erdem Ersoy (eersoy93)

Licensed with MIT license. See LICENSE file for full text.

